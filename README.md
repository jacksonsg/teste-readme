<!--
Olá Aprendiz e bem-vindo ao seu readme, em bom protuguês leia-me!
É aqui que você fará registros importantíssimos do seu progresso no desenvolvimento do projeto de programação e também anotações que possam te ajudar no futuro.
Antes de começar vamos aprender um pouquinho sobre um redme a como usá-lo?

Assim como em linguagem HTML o um arquivo readme.md é escrito com marcações ou "markdowns". Ou seja, iremos utilizar tags para títulos e cabeçlhos (<h1>...</h1> <h2>...</h2> <h3>...</h3>), ênfase com negrito, itálico e riscado (<b>...</b> <i>...</i> <strike>...</strike>), Listas (<ol>...</ol> <ul>...</ul>), links (<a>...</a)>), citações (<blockquote>...</blockquote>), blocos de código (```...```), além de imagens, tabelas e mais!
Agora que temos uma ideia de quão poderoso nosso readme pode ser, que tal tentarmos seguir o modelo abaixo e completar com informações que já temos?
Ah, não se esqueça de salvar as alterações para continuar de onde paramos na próxima aula, beleza?
-->

<!-- LOGO DO PROJETO -->
<br />
<p>
  <img src="images/logo.png" alt="Logo" width="80" height="80">
  <h3>NOME DO PROJETO</h3>
  <p>
    Bem-vindo ao readme do Projeto [NOME DO PROJETO]
  </p>
</p>

<!-- SUMÁRIO -->
<details open="open">
  <summary>Sumário</summary>
  <ol>
    <li>
      <a href="#sobre-o-projeto">Sobre o Projeto</a>
      <ul>
        <li><a href="#desenvolvido-com">Desenvolvido com</a></li>
      </ul>
    </li>
    <li>
        <a href="#iniciando">Iniciando</a>
    </li>
    <li><a href="#exemplos-de-uso">Exemplos de Uso</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contato">Contato</a></li>
    <li><a href="#licenciamento">Licenciamento</a></li>
    <li>
        <a href="#notas">Notas</a>
        <ul>
            <li><a href="#aula-1">Aula 1</a></li>
            <li><a href="#aula-2">Aula 2</a></li>
            <li><a href="#aula-3">Aula 3</a></li>
        </ul>
    </li>
  </ol>
</details>

<!-- SOBRE O PROJETO -->
## Sobre o Projeto

![IMAGEM DA TELA DO PROJETO](images/screenshot.png)

<!-- Descreva com um a dois parágrafos sobre o que é seu projeto e o que ele faz. -->
Esta é a apresentação do meu projeto...
O projeto tem como objetivo...
A inspiração por trás...

Como esse projeto pode ajudar:
* Primeiro motivo
* Segundo motivo
* Terceiro motivo

<!-- DESENVOLVIDO COM -->
### Desenvolvido com

Esta seção contém a lista de frameworks ou outros adicionais/plugins utilizados durante o desenvolvimento do projeto. Examplos:
* [Bootstrap](https://getbootstrap.com)
* [JQuery](https://jquery.com)

<!-- Iniciando -->
## Iniciando

Nessa seção você pode ensinar como você iniciou seu projeto.

<!-- EXEMPLOS DE USO -->
## Exemplos de uso

Alguns exemplos interessantes e úteis sobre como seu projeto pode ser utilizado. Adicione blocos de códigos e, se necessário, imagens.

<!-- LICENCIAMENTO -->
## Licenciamento

Distribuído sob a licença MIT. Veja `LICENSE` para mais informações.

<!-- CONTATO -->
## Contato

Seu Nome – [@SeuNomeTwitter](https://twitter.com/...) – [@SeuNomeLinkedIn](https://linkedin.com/...) – SeuEmail@exemplo.com

[https://github.com/yourname/github-link](https://github.com/othonalberto/)

<!-- ANOTAÇÕES -->
## Notas

Este é seu espaço para anotações, fique a vontade para preencher com todo tipo de informação útil para você!

<!-- AULA 1 -->
## Aula 1

...

<!-- AULA 2 -->
## Aula 2

...

<!-- AULA 3 -->
## Aula 3

...

